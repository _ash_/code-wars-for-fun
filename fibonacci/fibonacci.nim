import bignum

# not working
proc memo(toGo: int, first: Int, second: Int) : Int =
    if toGo == 1: return second

    return memo(
        toGo - 1,
        second,
        first + second
    )

proc fibonacci(position: int): Int =
    return memo(position - 1, newInt("0"), newInt("1"))

echo fibonacci(10000)