<?php

function carry(int $toGo, $first, $second) {
    if ($toGo == 1) {
        return $second;
    }

    return carry(
        $toGo - 1,
        $second,
        gmp_add($first, $second)
    );
}

function f($position)
{
    return carry($position - 1, '0', '1');
}

$start = microtime(true);
$result = f(10000);
$end = microtime(true);

echo $result;
echo "\n\nTime: " . ($end - $start) * 1000 . " ms\n";