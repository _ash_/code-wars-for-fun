<?php

function fib(int $n) {
    if ($n < 2) return $n;

    $n -= 2;
    $first = '0';
    $second = '1';
    
    for ($i=0; $i < $n; $i++) {
        [$first, $second] = [$second, gmp_add($first, $second)];
    };

    return $second;
}

$start = microtime(true);
$result = fib(10000);
$end = microtime(true);

echo $result;
echo "\n\nTime: " . ($end - $start) * 1000 . " ms\n";